# honey-bee-comb-segmentation

Segmentation of honey bee comb. Training pipeline. Code for master thesis.

Inference pipeline for thesis submission: https://git.imp.fu-berlin.de/ivam98/honey-bee-comb-segmentation-inference

Main version that will be supported is here: https://github.com/IvanMatoshchuk/honeybee_cells_segmentation_inference


